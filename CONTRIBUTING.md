# aimsutils

## Style
Please try to be reasonably [PEP8](https://www.python.org/dev/peps/pep-0008/)-conform. No need for 100% scores, but readable code it the goal.

## Testing
Parts of the code are covered by unittests. Please make sure that you at least do not break any existing tests with your code. 

Even better, consider writing your own tests to make sure nobody is going to break your code.


