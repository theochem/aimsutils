# aimsutils

## Purpose
This is a python collection of tools for FHI-aims. Any contributions which might be re-usable are welcome. For more information on contributing, see [contributing](CONTRIBUTING.md).
