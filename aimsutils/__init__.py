import os
import re
import warnings
import functools
from copy import copy


def deprecated(message=None):
    """This is a decorator which can be used to mark functions as
    deprecated. It will result in a warning being emmitted when the
    function is used."""

    def decorator(func):
        @functools.wraps(func)
        def new_func(*args, **kwargs):
            warnings.simplefilter('always', DeprecationWarning)
            warnings.warn("Call to deprecated function {name}. {msg}".format(
                name=func.__name__, msg=message),
                          category=DeprecationWarning, stacklevel=2)
            warnings.simplefilter('default', DeprecationWarning)
            return func(*args, **kwargs)
        return new_func
    return decorator


def find_aimsout(outfile):
    """
    Check if the given path is a folder or a output file. If it is a folder,
    find AIMS-output and return the file-path.

    Parameters
    ----------
    outfile : str
        Path to check.

    Returns
    -------
    outfile : str
        Path to the AIMS output file.
    """
    if os.path.isdir(outfile):
        p = re.compile("Have a nice day.")
        filenames = [os.path.join(outfile, fn) for fn in next(
            os.walk(outfile))[2]]

        for file in filenames:
            with open(file, "r") as f:
                fstring = str(f.readlines())
                hitlist = re.findall(p, fstring)
                if len(hitlist) > 0:
                    outfile = file

    return outfile


def aims_calc_params(binary_path, species_path, mpiexe='mpirun', xc='blyp',
                     species='tight', numcpu='4', default='organic'):
    """
    Construct dictionary for ASE aims calculator with reasonable defaults
    for calculation.

    Parameters
    ----------
    binary_path :
        The absolute path to the AIMS binary.
    species_path :
        The absolute path to the AIMS species folder.
    xc :
        The xc functional to use.
    species :
        The FHIaims basis function type to use (e.g. light, tight, ...)
    numcpu :
        Number of CPUs for the run_command.
    default :
        Which set of default parameters to choose.
        * organic [default] - organic molecules
    """
    # available default sets
    defaults = {"organic": ""}

    # consistency checks
    if not os.path.isdir(species_path):
        raise RuntimeError("This is not a valid directory: {}".format(
            species_path))
    if mpiexe=='':
        aims_binary=binary_path + " > aims.out"
    else:
        aims_binary = "{0} -np {1} {2} > aims.out".format(mpiexe, numcpu, binary_path)

    avail_species = {
                     'lightsmall_194': "non-standard/lightsmall_194",
                     'light_194': "non-standard/light_194",
                     "light": "light",
                     "tight": "tight",
                     "cc.3": "non-standard/NAO-VCC-nZ/NAO-VCC-3Z",
                     "cc.4": "non-standard/NAO-VCC-nZ/NAO-VCC-4Z",
                     "cc.5": "non-standard/NAO-VCC-nZ/NAO-VCC-5Z",
                     "light.ext": "light.ext",
                     "6311g-pd": "non-standard/gaussian_tight_770/6-311PD_biggest",
                     "tight.ext": "tight.ext",
                     "tight_tier2": "tight_tier2",
                     'tier2_aug2': "non-standard/Tier2_aug2",
                     "aug-pc-0": "non-standard/polarization_consistent/aug-pc-0",
                     "aug-pc-1": "non-standard/polarization_consistent/aug-pc-1",
                     "pc-0": "non-standard/polarization_consistent/pc-0",
                     "pc-1": "non-standard/polarization_consistent/pc-1",
                     "pc-2": "non-standard/polarization_consistent/pc-2",
                     "TZVP": "non-standard/gaussian_tight_770/def2-TZVP",
                     "QZVP": "non-standard/gaussian_tight_770/def2-QZVP",
                     "cc-pVDZ": "non-standard/gaussian_tight_770/cc-pVDZ",
                     "cc-pVTZ": "non-standard/gaussian_tight_770/cc-pVTZ",
                     "custom_bohdan_halobenzenes": "custom_bohdan_halobenzenes",
		     "tight.t3": "tight.t3"
                     }

    species_string = os.path.join(species_path, avail_species[species])

    defaults["organic"] = {"run_command": aims_binary,
                           "xc": xc,
                           "spin": "collinear",
                           "default_initial_moment": 0.0,
                           "occupation_type": "gaussian 0.01",
                           "mixer": "pulay",
                           "n_max_pulay": "10",
                           "charge_mix_param": "0.5",
                           "sc_accuracy_rho": "1E-4",
                           "sc_accuracy_eev": "1E-2",
                           "sc_accuracy_etot": "1E-6",
                           "sc_iter_limit": "200",
                           "relativistic": "none",
                           "species_dir": species_string,
                           }

    # slightly different default for fodft 
    # (no initial moment, determined by fo-calculator)
    defaults["organic_fodft"] = copy(defaults["organic"])
    del defaults["organic_fodft"]["default_initial_moment"]
    
    return defaults[default]
