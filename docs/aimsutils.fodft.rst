aimsutils.fodft package
=======================

Submodules
----------

aimsutils.fodft.aims module
---------------------------

.. automodule:: aimsutils.fodft.aims
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: aimsutils.fodft
    :members:
    :undoc-members:
    :show-inheritance:
