.. aimsutils documentation master file, created by
   sphinx-quickstart2 on Tue May  3 15:10:27 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to aimsutils's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

