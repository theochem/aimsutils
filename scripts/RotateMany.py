#!/usr/bin/env python

from aimsutils.rotate.rotate2 import Rotations
import argparse
import os


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Get parameters for rotation")

    parser.add_argument("-f", "--folder",
                        help="The main calculation folder.",
                        dest="folder", type=str, default="./")

    arg = parser.parse_args()

    r = Rotations()
    cwd = os.getcwd()
    try:
        os.chdir(arg.folder)
        r.load_rotations()
        r.write_restartfile()
    finally:
        os.chdir(cwd)

    print("Done.")
