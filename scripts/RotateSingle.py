#!/usr/bin/env python

from aimsutils.rotate.rotate2 import Rotations
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Get parameters for rotation")

    parser.add_argument("rotation",
                        help="The rotation in euler angles or as quaternion",
                        type=float, nargs="+")
    parser.add_argument("-t", "--translation",
                        help="A translation vector for the rotated geometry.",
                        dest="trans", type=float, nargs="+", default=[0, 0, 0])

    arg = parser.parse_args()

    r = Rotations()
    if len(arg.rotation) ==4:
        arg.rotation = np.quaternion(arg.rotation[0], arg.rotation[1], arg.rotation[2], arg.rotation[3])
    r.add_rotation("./", arg.rotation, arg.trans)
    r.write_restartfile()
    print("Done.")
