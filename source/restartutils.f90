! This file is part of aimsrotate.
! (C) 2015 Christoph Schober

subroutine read_restart_cluster(n_states, n_basis, n_spin, restartfile, KS_eigenvector, KS_eigenvalue, occ_numbers)
implicit none

character(*) :: restartfile
real*8, dimension(n_states, n_spin, 1) :: KS_eigenvalue
real*8, dimension(n_basis, n_states, n_spin, 1)  :: KS_eigenvector
real*8, dimension(n_states, n_spin, 1)   :: occ_numbers
integer :: n_basis, n_states, n_spin 
integer :: i_basis, i_states, i_spin

!f2py intent(in) n_states
!f2py intent(in) n_basis
!f2py intent(in) n_spin

!f2py intent(in) restartfile
!f2py intent(out) KS_eigenvalue
!f2py intent(out) KS_eigenvector
!f2py intent(out) occ_numbers

open(file = restartfile, unit = 20, status = 'unknown', form = 'unformatted')

read(20) n_basis
read(20) n_states
read(20) n_spin

! KS_eigenvector(n_basis,n_states,n_spin,n_k_points_task)
do i_basis = 1, n_basis
    do i_states = 1, n_states
        do i_spin = 1, n_spin
            read(20) KS_eigenvector(i_basis,i_states,i_spin,1)
        end do
    end do
end do

do i_states = 1, n_states
    do i_spin = 1, n_spin
        read(20) KS_eigenvalue(i_states,i_spin,1), occ_numbers(i_states,i_spin,1)
    end do
end do

close(unit = 20)

end subroutine read_restart_cluster

subroutine read_restart_periodic(n_states, n_basis, n_spin, restartfile, KS_eigenvector, KS_eigenvalue, occ_numbers)
! At the moment this subroutine is almost identical with read_restart_cluster.
! This could change if we add support for periodic calculations with more than
! the gamma point

implicit none

character(*) :: restartfile
real*8, dimension(n_states, n_spin, 1) :: KS_eigenvalue
!complex*16, dimension(n_basis, n_states, n_spin, 1)  :: KS_eigenvector
real*8, dimension(n_basis, n_states, n_spin, 1)  :: KS_eigenvector
real*8, dimension(n_states, n_spin, 1)   :: occ_numbers
integer :: n_basis, n_states, n_spin 
integer :: i_basis, i_states, i_spin, i_k
integer :: n_k

!f2py intent(in) n_states
!f2py intent(in) n_basis
!f2py intent(in) n_spin

!f2py intent(in) restartfile
!f2py intent(out) KS_eigenvalue
!f2py intent(out) KS_eigenvector
!f2py intent(out) occ_numbers

open(file = restartfile, unit = 20, status = 'unknown', form = 'unformatted')

read(20) n_basis
read(20) n_states
read(20) n_spin
read(20) n_k

! KS_eigenvector(n_basis,n_states,n_spin,n_k_points_task)
do i_k = 1, n_k
    do i_spin = 1, n_spin
        do i_states = 1, n_states
            do i_basis = 1, n_basis
               read(20) KS_eigenvector(i_basis,i_states,i_spin,1)
            end do
        end do
    end do
end do

do i_k = 1, n_k
    do i_spin = 1, n_spin
        do i_states = 1, n_states
            read(20) KS_eigenvalue(i_states,i_spin,1), occ_numbers(i_states,i_spin,1)
        end do
    end do
end do

close(unit = 20)

end subroutine read_restart_periodic

subroutine write_formatted_restart_cluster(restartfile, KS_eigenvector, KS_eigenvalue, &
               occ_numbers, n_states, n_basis, n_spin)
implicit none

character(*) :: restartfile
real*8, dimension(n_states, n_spin, 1) :: KS_eigenvalue
real*8, dimension(n_basis, n_states, n_spin, 1)  :: KS_eigenvector
real*8, dimension(n_states, n_spin, 1)   :: occ_numbers
integer :: n_basis, n_states, n_spin 
integer :: i_basis, i_states, i_spin

!f2py intent(in) n_states
!f2py intent(in) n_basis
!f2py intent(in) n_spin

!f2py intent(in) restartfile
!f2py intent(in) KS_eigenvalue
!f2py intent(in) KS_eigenvector
!f2py intent(in) occ_numbers

open(file = restartfile, unit = 20, status = 'unknown', form = 'formatted')

write(20,*) n_basis
write(20,*) n_states
write(20,*) n_spin


do i_basis = 1, n_basis
    do i_states = 1, n_states
        do i_spin = 1, n_spin
            write(20,*) KS_eigenvector(i_basis,i_states,i_spin,1)
        end do
    end do
end do

! Write eigenvalues and occupation numbers.
do i_states = 1, n_states
    do i_spin = 1, n_spin
        write(20,*) KS_eigenvalue(i_states,i_spin,1), &
                      occ_numbers(i_states,i_spin,1)
    end do
end do

close(unit = 20)

end subroutine write_formatted_restart_cluster

subroutine write_restart_cluster(restartfile, KS_eigenvector, KS_eigenvalue, &
               occ_numbers, n_states, n_basis, n_spin)
implicit none

character(*) :: restartfile
real*8, dimension(n_states, n_spin, 1) :: KS_eigenvalue
real*8, dimension(n_basis, n_states, n_spin, 1)  :: KS_eigenvector
real*8, dimension(n_states, n_spin, 1)   :: occ_numbers
integer :: n_basis, n_states, n_spin 
integer :: i_basis, i_states, i_spin

!f2py intent(in) n_states
!f2py intent(in) n_basis
!f2py intent(in) n_spin

!f2py intent(in) restartfile
!f2py intent(in) KS_eigenvalue
!f2py intent(in) KS_eigenvector
!f2py intent(in) occ_numbers

open(file = restartfile, unit = 20, status = 'unknown', form = 'unformatted')

write(20) n_basis
write(20) n_states
write(20) n_spin


do i_basis = 1, n_basis
    do i_states = 1, n_states
        do i_spin = 1, n_spin
            write(20) KS_eigenvector(i_basis,i_states,i_spin,1)
        end do
    end do
end do

! Write eigenvalues and occupation numbers.
do i_states = 1, n_states
    do i_spin = 1, n_spin
        write(20) KS_eigenvalue(i_states,i_spin,1), &
                      occ_numbers(i_states,i_spin,1)
    end do
end do

close(unit = 20)

end subroutine write_restart_cluster


subroutine write_restart_periodic(restartfile, KS_eigenvector, &
                KS_eigenvalue, occ_numbers, n_basis, n_states, n_spin)
! At the moment this subroutine is almost identical with read_restart_cluster.
! This could change if we add support for periodic calculations with more than
! the gamma point

implicit none

character(*) :: restartfile
real*8, dimension(n_states, n_spin, 1) :: KS_eigenvalue
!complex*16, dimension(n_basis, n_states, n_spin, 1)  :: KS_eigenvector
real*8, dimension(n_basis, n_states, n_spin, 1)  :: KS_eigenvector
real*8, dimension(n_states, n_spin, 1)   :: occ_numbers
integer :: n_basis, n_states, n_spin 
integer :: i_basis, i_states, i_spin, i_k
integer :: n_k

!f2py intent(in) n_states
!f2py intent(in) n_basis
!f2py intent(in) n_spin

!f2py intent(in) restartfile
!f2py intent(in) KS_eigenvalue
!f2py intent(in) KS_eigenvector
!f2py intent(in) occ_numbers

open(file = restartfile, unit = 20, status = 'unknown', form = 'unformatted')

! n_k is one as long as only gamma point is allowed.
n_k = 1

write(20) n_basis
write(20) n_states
write(20) n_spin
write(20) n_k

do i_k = 1, n_k
do i_spin = 1, n_spin
    do i_states = 1, n_states
        do i_basis = 1, n_basis
            write(20) KS_eigenvector(i_basis,i_states,i_spin,i_k)
        end do ! i_basis
    end do ! i_states
end do ! i_spin
end do ! i_k

! Write KS_eigenvalues occupation numbers
do i_k = 1,n_k
do i_spin = 1, n_spin
    do i_states = 1, n_states
        write(20) KS_eigenvalue(i_states,i_spin,i_k), &
                 occ_numbers(i_states,i_spin,i_k)
    end do ! i_states
end do ! i_spin
end do ! i_k

close(unit = 20)

end subroutine write_restart_periodic
