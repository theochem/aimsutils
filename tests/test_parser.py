#!/usr/bin/env python
import unittest
# from aimsrotate.aims import data, utils
# from aimsrotate import wigner, rotate
import os
import tempfile
import shutil
from collections import OrderedDict

from aimsutils import parser, ylms


class TestParser(unittest.TestCase):
    def setUp(self):
        self.cwd = os.getcwd()
        self.tmpdir = tempfile.mkdtemp()
        os.chdir(self.tmpdir)
        self.outfile_cluster = os.path.join(self.cwd, 'tests', 'data',
                                            'h2o.cluster.1',
                                            'aims.out')
        self.outfile_periodic = os.path.join(self.cwd, 'tests', 'data',
                                             'h2o.periodic.2',
                                             'aims.out')
        self.main_folder = os.path.join(self.cwd, 'tests', 'data',
                                        'h2o.periodic.1')

    def test_parse_cluster_aimsout(self):
        reference_meta = {'n_basis': 43,
                          'n_empty': 6,
                          'n_spin': 1,
                          'n_states': 11,
                          'n_states_saved': 8,
                          'periodic': False,
                          'restartfile': 'h_kv'}
        meta = parser.parse_aimsout(self.outfile_cluster)
        self.assertEqual(meta, reference_meta)

    def test_parse_periodic_aimsout(self):
        reference_meta = {'n_basis': 43,
                          'n_empty': 6,
                          'n_spin': 2,
                          'n_states': 11,
                          'n_states_saved': 8,
                          'periodic': True,
                          'restartfile': 'h_kv001',
                          'proc_gammaK': 1}

        meta = parser.parse_aimsout(self.outfile_periodic)
        self.assertEqual(meta, reference_meta)

    def test_parse_basis_outfile(self):
        reference_basis = OrderedDict(
            [(1, [1, 1, 0, 0]), (2, [1, 3, 0, 0]), (3, [1, 2, 0, 0]),
             (4, [1, 1, 0, 0]), (5, [1, 3, 1, -1]), (6, [1, 3, 1, 0]),
             (7, [1, 3, 1, 1]), (8, [1, 2, 1, -1]), (9, [1, 2, 1, 0]),
             (10, [1, 2, 1, 1]), (11, [1, 2, 1, -1]), (12, [1, 2, 1, 0]),
             (13, [1, 2, 1, 1]), (14, [1, 3, 2, -2]), (15, [1, 3, 2, -1]),
             (16, [1, 3, 2, 0]), (17, [1, 3, 2, 1]), (18, [1, 3, 2, 2]),
             (19, [1, 3, 2, -2]), (20, [1, 3, 2, -1]), (21, [1, 3, 2, 0]),
             (22, [1, 3, 2, 1]), (23, [1, 3, 2, 2]), (24, [1, 4, 3, -3]),
             (25, [1, 4, 3, -2]), (26, [1, 4, 3, -1]), (27, [1, 4, 3, 0]),
             (28, [1, 4, 3, 1]), (29, [1, 4, 3, 2]), (30, [1, 4, 3, 3]),
             (31, [1, 5, 4, -4]), (32, [1, 5, 4, -3]), (33, [1, 5, 4, -2]),
             (34, [1, 5, 4, -1]), (35, [1, 5, 4, 0]), (36, [1, 5, 4, 1]),
             (37, [1, 5, 4, 2]), (38, [1, 5, 4, 3]), (39, [1, 5, 4, 4]),
             (40, [2, 1, 0, 0]), (41, [2, 2, 0, 0]), (42, [3, 1, 0, 0]),
             (43, [3, 2, 0, 0])])
        basis = parser.parse_basis(os.path.join(self.main_folder,
                                                "basis-indices.out"))
        self.assertEqual(basis, reference_basis)

    def test_lvec_generation(self):
        reference_lvec = [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2,
                          2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4,
                          4, 4, 4, 0, 0, 0, 0]
        basis = parser.parse_basis(os.path.join(self.main_folder,
                                                "basis-indices.out"))

        lvec = ylms.get_l_vector(basis)
        self.assertEqual(lvec, reference_lvec)

    def tearDown(self):
        os.chdir(self.cwd)
        shutil.rmtree(self.tmpdir)
